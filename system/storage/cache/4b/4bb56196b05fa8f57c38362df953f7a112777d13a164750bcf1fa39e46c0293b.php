<?php

/* common/header.twig */
class __TwigTemplate_8f032e01479327f0efb74f0b5c45e2cda160aa79e5b8a3a6abf61509a3b852c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html dir=\"";
        // line 2
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<head>
  <meta charset=\"UTF-8\" />
  <title>";
        // line 5
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
  <base href=\"";
        // line 6
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
  ";
        // line 7
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 8
            echo "  <meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
  ";
        }
        // line 10
        echo "  ";
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 11
            echo "  <meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
  ";
        }
        // line 13
        echo "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0\" />
  <script type=\"text/javascript\" src=\"view/javascript/jquery/jquery-2.1.1.min.js\"></script>
  <script type=\"text/javascript\" src=\"view/javascript/bootstrap/js/bootstrap.min.js\"></script>
  <link href=\"view/stylesheet/bootstrap.css\" type=\"text/css\" rel=\"stylesheet\" />
  <link href=\"view/javascript/font-awesome/css/font-awesome.min.css\" type=\"text/css\" rel=\"stylesheet\" />
  <script src=\"view/javascript/jquery/datetimepicker/moment/moment.min.js\" type=\"text/javascript\"></script>
  <script src=\"view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js\" type=\"text/javascript\"></script>
  <script src=\"view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js\" type=\"text/javascript\"></script>
  <link href=\"view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
  <link type=\"text/css\" href=\"view/stylesheet/stylesheet.css?5\" rel=\"stylesheet\" media=\"screen\" />
  <link href=\"/image/catalog/favicon.ico?1\" rel=\"icon\" />
  ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 25
            echo "  <link type=\"text/css\" href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 28
            echo "  <link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "  <script src=\"view/javascript/common.js\" type=\"text/javascript\"></script>
  ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 32
            echo "  <script type=\"text/javascript\" src=\"";
            echo $context["script"];
            echo "\"></script>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
  <script src=\"../catalog/view/javascript/jquery/jquery.mask.js\" type=\"text/javascript\"></script>
  <script type=\"text/javascript\">
  \$(document).ready(function(){    
    \$(\".maskPhone, #input-telephone\").mask(\"(99) 99999-9999\");
    \$(\".maskPhone\").on(\"blur\", function() {
      var last = \$(this).val().substr( \$(this).val().indexOf(\"-\") + 1 );

      if( last.length == 3 ) {
        var move = \$(this).val().substr( \$(this).val().indexOf(\"-\") - 1, 1 );
        var lastfour = move + last;
        var first = \$(this).val().substr( 0, 9 );

        \$(this).val( first + '-' + lastfour );
      }
    });

    \$(\"#input-price, .inputValuePrice, #form-payment #input-total, #form-shipping #input-cost, #form-shipping #input-total\").mask('#####.##', {reverse: true});
    \$('#input-custom-field1').mask('000.000.000-00', {reverse: true});
  });
  </script>
  <script type=\"text/javascript\" src=\"view/javascript/stringToSlug/jquery.stringToSlug.min.js\"></script>
  <script type=\"text/javascript\" src=\"view/javascript/stringToSlug/speakingurl.js\"></script>

</head>
<body>
  <div id=\"container\">
    <header id=\"header\" class=\"navbar navbar-static-top\">
      <div class=\"container-fluid\">


        <div id=\"header-logo\" class=\"navbar-header\"><a href=\"";
        // line 65
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\" class=\"navbar-brand\"><img src=\"view/image/logo.png\" alt=\"";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "\" title=\"";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "\" /></a></div>
        <a href=\"#\" id=\"button-menu\" class=\"hidden-md hidden-lg\"><span class=\"fa fa-bars\"></span></a>";
        // line 66
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 67
            echo "
        <ul class=\"nav navbar-nav navbar-right\">
          <li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"";
            // line 69
            echo (isset($context["image"]) ? $context["image"] : null);
            echo "\" alt=\"";
            echo (isset($context["firstname"]) ? $context["firstname"] : null);
            echo "\" title=\"";
            echo (isset($context["firstname"]) ? $context["firstname"] : null);
            echo "\" id=\"user-profile\" class=\"img-circle\" />";
            echo (isset($context["firstname"]) ? $context["firstname"] : null);
            echo " <i class=\"fa fa-caret-down fa-fw\"></i></a>
            <ul class=\"dropdown-menu dropdown-menu-right\">
              <li><a href=\"";
            // line 71
            echo (isset($context["profile"]) ? $context["profile"] : null);
            echo "\"><i class=\"fa fa-user-circle-o fa-fw\"></i> ";
            echo (isset($context["text_profile"]) ? $context["text_profile"] : null);
            echo "</a></li>
              <li role=\"separator\" class=\"divider\"></li>
              <li class=\"dropdown-header\">";
            // line 73
            echo (isset($context["text_store"]) ? $context["text_store"] : null);
            echo "</li>
              ";
            // line 74
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
                // line 75
                echo "              <li><a href=\"";
                echo $this->getAttribute($context["store"], "href", array());
                echo "\" target=\"_blank\">";
                echo $this->getAttribute($context["store"], "name", array());
                echo "</a></li>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "            </ul>
          </li>
          <li><a href=\"";
            // line 79
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</span></a></li>
        </ul>
        ";
        }
        // line 81
        echo " </div>
      </header>
";
    }

    public function getTemplateName()
    {
        return "common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 81,  205 => 79,  201 => 77,  190 => 75,  186 => 74,  182 => 73,  175 => 71,  164 => 69,  160 => 67,  158 => 66,  150 => 65,  117 => 34,  108 => 32,  104 => 31,  101 => 30,  90 => 28,  85 => 27,  72 => 25,  68 => 24,  55 => 13,  49 => 11,  46 => 10,  40 => 8,  38 => 7,  34 => 6,  30 => 5,  22 => 2,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <head>*/
/*   <meta charset="UTF-8" />*/
/*   <title>{{ title }}</title>*/
/*   <base href="{{ base }}" />*/
/*   {% if description %}*/
/*   <meta name="description" content="{{ description }}" />*/
/*   {% endif %}*/
/*   {% if keywords %}*/
/*   <meta name="keywords" content="{{ keywords }}" />*/
/*   {% endif %}*/
/*   <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />*/
/*   <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>*/
/*   <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>*/
/*   <link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />*/
/*   <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />*/
/*   <script src="view/javascript/jquery/datetimepicker/moment/moment.min.js" type="text/javascript"></script>*/
/*   <script src="view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js" type="text/javascript"></script>*/
/*   <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>*/
/*   <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />*/
/*   <link type="text/css" href="view/stylesheet/stylesheet.css?5" rel="stylesheet" media="screen" />*/
/*   <link href="/image/catalog/favicon.ico?1" rel="icon" />*/
/*   {% for style in styles %}*/
/*   <link type="text/css" href="{{ style.href }}" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/*   {% endfor %}*/
/*   {% for link in links %}*/
/*   <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/*   {% endfor %}*/
/*   <script src="view/javascript/common.js" type="text/javascript"></script>*/
/*   {% for script in scripts %}*/
/*   <script type="text/javascript" src="{{ script }}"></script>*/
/*   {% endfor %}*/
/* */
/*   <script src="../catalog/view/javascript/jquery/jquery.mask.js" type="text/javascript"></script>*/
/*   <script type="text/javascript">*/
/*   $(document).ready(function(){    */
/*     $(".maskPhone, #input-telephone").mask("(99) 99999-9999");*/
/*     $(".maskPhone").on("blur", function() {*/
/*       var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );*/
/* */
/*       if( last.length == 3 ) {*/
/*         var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );*/
/*         var lastfour = move + last;*/
/*         var first = $(this).val().substr( 0, 9 );*/
/* */
/*         $(this).val( first + '-' + lastfour );*/
/*       }*/
/*     });*/
/* */
/*     $("#input-price, .inputValuePrice, #form-payment #input-total, #form-shipping #input-cost, #form-shipping #input-total").mask('#####.##', {reverse: true});*/
/*     $('#input-custom-field1').mask('000.000.000-00', {reverse: true});*/
/*   });*/
/*   </script>*/
/*   <script type="text/javascript" src="view/javascript/stringToSlug/jquery.stringToSlug.min.js"></script>*/
/*   <script type="text/javascript" src="view/javascript/stringToSlug/speakingurl.js"></script>*/
/* */
/* </head>*/
/* <body>*/
/*   <div id="container">*/
/*     <header id="header" class="navbar navbar-static-top">*/
/*       <div class="container-fluid">*/
/* */
/* */
/*         <div id="header-logo" class="navbar-header"><a href="{{ home }}" class="navbar-brand"><img src="view/image/logo.png" alt="{{ heading_title }}" title="{{ heading_title }}" /></a></div>*/
/*         <a href="#" id="button-menu" class="hidden-md hidden-lg"><span class="fa fa-bars"></span></a>{% if logged %}*/
/* */
/*         <ul class="nav navbar-nav navbar-right">*/
/*           <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ image }}" alt="{{ firstname }}" title="{{ firstname }}" id="user-profile" class="img-circle" />{{ firstname }} <i class="fa fa-caret-down fa-fw"></i></a>*/
/*             <ul class="dropdown-menu dropdown-menu-right">*/
/*               <li><a href="{{ profile }}"><i class="fa fa-user-circle-o fa-fw"></i> {{ text_profile }}</a></li>*/
/*               <li role="separator" class="divider"></li>*/
/*               <li class="dropdown-header">{{ text_store }}</li>*/
/*               {% for store in stores %}*/
/*               <li><a href="{{ store.href }}" target="_blank">{{ store.name }}</a></li>*/
/*               {% endfor %}*/
/*             </ul>*/
/*           </li>*/
/*           <li><a href="{{ logout }}"><i class="fa fa-sign-out"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_logout }}</span></a></li>*/
/*         </ul>*/
/*         {% endif %} </div>*/
/*       </header>*/
/* */
